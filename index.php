<?php

require("config.php");

session_start();

define('ACTIVE_SESSION_FILE', 'active_session.php');

if (($session_access_time = filemtime(ACTIVE_SESSION_FILE)) + 60 * 10 < time()) {
	file_put_contents(ACTIVE_SESSION_FILE, '<?');
}

$logged_session_id = require ACTIVE_SESSION_FILE;

function save_session_id($id) {
	file_put_contents(ACTIVE_SESSION_FILE, '<? define("ACTIVE_SESSION", "' . $id .'"); ?>');
}


if(!defined('ACTIVE_SESSION')) {
	session_unset();
}

if(defined('ACTIVE_SESSION') && ACTIVE_SESSION != session_id()) {
        die('Another user is already logged in');
}

if($session_access_time + 10 > time()) {
	$left = 10 - (time() - $session_access_time);
	die("Too often. try again in {$left} seconds.<script>setTimeout(()=>location.reload(),{$left} * 1000);</script>");
}

touch(ACTIVE_SESSION_FILE);

//Global Variables and constants

define('MODEM_LSUSB_SIERRA', '1199:9071');
define('MODEM_LSUSB_EC25QL', '2C7C:0125');

$GLOBALS['modem_type'] = 'unknown';



if(!$_SESSION['auth'])
{

	if(empty($_POST["login"]) || empty($_POST["pass"]) ) {
	?>
	<html>
	<head>
    <style>

input[type=text], input[type=password] , input[type=submit]  {
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    resize: vertical;
    font-size: 14px;
       text-align: center;
}
    </style>
	</head>
	<body>
<script src="jquery.js">
</script>
	<script>
function validateLoginEditBox(element, name, errname)
{
	if( /[^a-zA-Z0-9]/.test( element) ) {
      	$("#"+errname).html('Please enter only alphabet and numbers to ' + name + ' !');
      	$("#"+name).css("border", "1px solid red");
     	//  $("#"+name).addClass("error");
       	return false;
    }

    if(element.length < 8 || element.length > 12 )
    {
		$("#"+errname).html('Please enter ' + name + ' name between 8 and 12 symbols !');
      	$("#"+name).css("border", "1px solid red");
     	//  $("#"+name).addClass("error");
       	return false;
    }

	$("#"+name).css("border", "1px solid #ccc");
    $("#errdesc").html('');

	return true;
}
function validateLogin(f)
{
	if (validateLoginEditBox(document.getElementById('login').value, 'login', 'errdesc') === true &&
		validateLoginEditBox(document.getElementById('password').value, 'password', 'errdesc') === true)
	  f.submit();
}
	</script>
    <center>
    	<div style="margin-top: 75px; ">
    	<table align="center"><tr><td width="100"></td><td><h3 style="color: orange; font-size: 44px;">webPanel</h3> <br><br><br><span id="errdesc" style="color: red"></span> <br><form method="POST" onsubmit="validateLogin(this);return false;"></td></tr>
    	<tr><td width="100"><label for="login">Login: </label></td><td><input type="text" id="login" name="login" value=""></td></tr>
        <tr><td width="100"><label for="pass">Password : </label></td><td><input type="password" id="password" name="pass" value=""></td></tr>
        <tr><td colspan="2" align="center" style="text-align: center; "><br><center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="Login" style="background-color: lightblue; padding: 7 35 7 35;"></center></td></tr>
        </table>
    	</form>
    	</div>
    </center>
	</body>
	</html>

	<?php
	exit;
   }
   else
   {
   	if($_POST["login"]==$login && $_POST["pass"]==$pass)
   	{
   		$_SESSION['auth']=1;
		save_session_id(session_id());
   	}
   	else
   	{
?>

	<html>
	<head>
    <style>

input[type=text], input[type=password] , input[type=submit]  {
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    resize: vertical;
    font-size: 14px;
       text-align: center;
}
    </style>
	</head>
	<body>
<script src="jquery.js">
</script>
	<script>
function validateLoginEditBox(element, name, errname)
{
	if( /[^a-zA-Z0-9]/.test( element) ) {
      	$("#"+errname).html('Please enter only alphabet and numbers to ' + name + ' !');
      	$("#"+name).css("border", "1px solid red");
     	//  $("#"+name).addClass("error");
       	return false;
    }

    if(element.length < 8 || element.length > 12 )
    {
		$("#"+errname).html('Please enter ' + name + ' name between 8 and 12 symbols !');
      	$("#"+name).css("border", "1px solid red");
     	//  $("#"+name).addClass("error");
       	return false;
    }

	$("#"+name).css("border", "1px solid #ccc");
    $("#errdesc").html('');

	return true;
}
function validateLogin(f)
{
	if (validateLoginEditBox(document.getElementById('login').value, 'login', 'errdesc') === true &&
		validateLoginEditBox(document.getElementById('password').value, 'password', 'errdesc') === true)
	  f.submit();
}
</script>
    <center>

    	<div style="margin-top: 75px; ">
    	<table align="center"><tr><td width="100"></td><td><h3 style="color: orange; font-size: 44px;">webPanel</h3> <br><br><br><span id="errdesc" style="color: red">Wrong login or password!</span> <br><form method="POST" onsubmit="validateLogin(this);return false;"></td></tr>
    	<tr><td width="100"><label for="login">Login: </label></td><td><input type="text" id="login" name="login" value=""></td></tr>
        <tr><td width="100"><label for="pass">Password : </label></td><td><input type="password" id="password" name="pass" value=""></td></tr>
        <tr><td colspan="2" align="center" style="text-align: center; "><br><center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="Login" style="background-color: lightblue; padding: 7 35 7 35;"></center></td></tr>
        </table>
    	</form>
    	</div>


    </center>
	</body>
	</html>

<?php
   		exit;
   	}
   }
}

if(!$_SESSION['first'])
{

}

$_SESSION['first']=1;

include "php_serial.class.php";
error_reporting(0);



// Let's start the class
$serial = new phpSerial;


$dev = "/dev/ttyUSB2";
class LTE_MODEM {
	public $status = '';
	public $apn = '';
	public $signal_strength = '';
	public $gps = '';
	public $band = '';
	public $bandwidth = '';
	public $network = '';
	public $ip_address='';
	public $number='';
}

$ltemodem = new LTE_MODEM;

$savedconfig=unserialize(file_get_contents('panel.conf'));

$command="/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'";
$ethIP = exec ($command);

function csq_human_readible( $status ){
	$str_status = strval($status).' ';
	if( $status >= 20 ){
		$str_status .= 'Excellent';
	}
	elseif( $status >= 15 ){
		$str_status .= 'Good';
	}
	elseif( $status >= 10 ){
		$str_status .= 'OK';
	}
	elseif( $status >= 2 ){
		$str_status .= 'Marginal';
	}
	else{
		$str_status .= 'No signal';
	}

	return $str_status;
}

function get_modem_status() {
	global $ltemodem;

	require 'modem_status.php';
	  $ltemodem->status = 'UNKNOWN CPIN';
	if (defined('MODEM_STATUS')) {
		if (strpos(MODEM_STATUS, '+CPIN: READY') !== -1) {
			$ltemodem->status = 'READY';
		}else if (strpos(MODEM_STATUS, '+CME ERROR: SIM not inserted') !== -1){
			$ltemodem->status = 'SIM NOT INSERTED';
		} else if (strpos(MODEM_STATUS, '+CPIN: SIM PIN') !== -1) {
			$ltemodem->status = 'DISABLE SIM PIN & TRY AGAIN';
		}else {
			$ltemodem->status = 'UNKNOWN CPIN';
		}

		return $ltemodem->status;
	}
}

function get_modem_type () {

	require 'modem_status.php';
  $GLOBALS['modem_type'] = 'unknown';
	if (defined('MODEM_TYPE')) {
		$GLOBALS['modem_type'] = MODEM_TYPE;
		return;
	}
}

function LTE_MODEM_INFORMATION() {
	global $serial;
	global $ltemodem;
	global $savedconfig;

	// if setting from modem different from config
    $changed=0;
	/*
	at!gstatus?
	!GSTATUS:
	Current Time:  739              Temperature: 25
	Reset Counter: 1                Mode:        ONLINE
	System mode:   LTE              PS state:    Attached
	LTE band:      B3               LTE bw:      15 MHz
	LTE Rx chan:   1392             LTE Tx chan: 19392
	LTE CA state:  NOT ASSIGNED
	EMM state:     Registered       Normal Service
	RRC state:     RRC Idle
	IMS reg state: No Srv

	PCC RxM RSSI:  -79              RSRP (dBm):  -107
	PCC RxD RSSI:  -96              RSRP (dBm):  -137
	Tx Power:      0                TAC:         0C96 (3222)
	RSRQ (dB):     -9.3             Cell ID:     0007CD00 (511232)
	SINR (dB):      5.2
	*/
	/*
	at!gstatus?
	!GSTATUS:
	Current Time:  888              Temperature: 33
	Reset Counter: 1                Mode:        ONLINE
	System mode:   WCDMA            PS state:    Attached
	WCDMA band:    WCDMA 2100
	WCDMA channel: 10837
	GMM (PS) state:REGISTERED       NORMAL SERVICE
	MM (CS) state: IDLE             NORMAL SERVICE

	WCDMA L1 state:L1M_FACH         LAC:         825C (33372)
	RRC state:   CELL_FACH          Cell ID:     00256CF8 (2452728)
	RxM RSSI C0:   -68              RxD RSSI C0:  ---
	RxM RSSI C1:    ---             RxD RSSI C1:  ---
	*/

    if($GLOBALS['modem_type'] == 'sierra')
    {
        $lteconf["STATUS"]=$ltemodem->status;

        $serial->sendMessage("at!gstatus?\r");
        $read = $serial->readPort();
        $marker = 'System mode:';
        $pos = strpos($read, $marker);

        if ($pos != false) {
            $rest = substr($read, $pos+strlen($marker));
            $pieces = explode(" ", ltrim($rest));
            $ltemodem->network = $pieces[0];

        }
        if ($ltemodem->network == 'LTE') {
            $lte_band = 'LTE band:';
            $lte_bw = 'LTE bw:';
            $pos = strpos($read, $lte_bw);
            if ($pos != false) {
                $rest = substr($read, $pos+strlen($lte_bw));
                $pieces = explode(" ", ltrim($rest));

                $ltemodem->bandwidth = $pieces[0] . " " .  $pieces[1];
                {
                    $savedconfig["BANDWIDTH"] = $ltemodem->bandwidth;
                    $changed=1;
                }

                $pos = strpos($read, $lte_band);
                $rest = substr($read, $pos+strlen($lte_band));
                $pieces = explode(" ", ltrim($rest));
                $ltemodem->band = $pieces[0];

                if(!empty($ltemodem->band) && $ltemodem->band !== $savedconfig["BAND"] )
                {
                    $savedconfig["BAND"] = $ltemodem->band;
                    $changed=1;
                }
            }
        }elseif($ltemodem->network == 'WCDMA') {
            // bandwidth is not present in the wcdma response
            $lte_bw = 'unknown';
            $lte_band = 'WCDMA band:';
            $pos = strpos($read, $lte_band);
            $rest = substr($read, $pos+strlen($lte_band));
            $pieces = explode(" ", ltrim($rest));
            $ltemodem->band = $pieces[1];

            if(!empty($ltemodem->band) && $ltemodem->band !== $savedconfig["BAND"] )
            {
                $savedconfig["BAND"] = $ltemodem->band;
                $changed=1;
            }
            $ltemodem->bandwidth = $lte_bw;
            if(!empty($ltemodem->bandwidth) && ( $ltemodem->bandwidth !== $savedconfig["BANDWIDTH"] ) )
            {
                $savedconfig["BANDWIDTH"] = $ltemodem->bandwidth;
                $changed=1;
            }

        } else {
            // what if it's neither LTE, nor WCDMA?
            $ltemodem->band      = 'UNKNOWN';
            $ltemodem->bandwidth = 'UNKNOWN';
            $ltemodem->network   = 'UNKNOWN';
						if(!empty($ltemodem->bandwidth) && ( $ltemodem->bandwidth !== $savedconfig["BANDWIDTH"] ) )
            {
                $savedconfig["BANDWIDTH"] = $ltemodem->bandwidth;
                $changed=1;
            }
						if(!empty($ltemodem->band) && $ltemodem->band !== $savedconfig["BAND"] )
            {
                $savedconfig["BAND"] = $ltemodem->band;
                $changed=1;
            }
						if(!empty($ltemodem->bandwidth) && ( $ltemodem->bandwidth !== $savedconfig["BANDWIDTH"] ) )
            {
                $savedconfig["BANDWIDTH"] = $ltemodem->bandwidth;
                $changed=1;
            }
				}

        /*
        at+cgdcont?
        +CGDCONT: 1,"IP","three.co.uk","0.0.0.0",0,0,0,0
        */
        $serial->sendMessage("at+cgdcont?\r");
        $read = $serial->readPort();

        $marker = '+CGDCONT:';
        $pos = strpos($read, $marker);
        if ($pos != false) {

            $pieces_q = explode(",",$read);
            $pieces=explode("\"",$pieces_q[2]);

            $ltemodem->apn = $pieces[1];

            if(!empty($ltemodem->apn) && ( $ltemodem->apn !== $savedconfig["APN"] ) )
            {
                $savedconfig["APN"] = $ltemodem->apn;
                $changed=1;
            }
        }

        /*
        at+cnum?
        Mobile number display
        */

        $serial->sendMessage("at+cnum\r");
        $read = $serial->readPort();
        $marker = '+CNUM:';
        $pos = strpos($read, $marker);
        if( $pos != false ){
            $rest = substr($read, $pos + strlen($marker));
            $ltemodem->number = str_replace('"','',explode(',',$rest)[1]);
        }

        /*
        at+cops?
        +cops: 0,0,"3”,7
        this display the network in this case network is 3 sometimes it will be t-mobile etc.
        */
        $serial->sendMessage("at+cops?\r");
        $read = $serial->readPort();
        //var_dump($read);
        $marker = '+cops:';
        $pos = strpos($read, $marker);
        if ($pos != false) {
            $rest = substr($read, $pos+strlen($marker));
            $pieces = explode("\"", $rest);
            //if ($ltemodem->network != 'LTE') {
            $ltemodem->network = $ltemodem->network . " "  . $pieces[1] . ' ( '.$ltemodem->number.' )';

            if(!empty($ltemodem->network) && ( $ltemodem->network !== $savedconfig["Network"] ) )
            {
                $savedconfig["Network"] = $ltemodem->network;
                $changed=1;
            }
            //}
        }

        /*
        at+cgpaddr=1
        +CGPADDR: 1,10.135.229.42
        this command give IP address
        */
        $serial->sendMessage("at+cgpaddr=1\r");
        $read = $serial->readPort();
        $marker = '+CGPADDR:';
        $pos = strpos($read, $marker);
        if ($pos != false) {
            $rest = substr($read, $pos+strlen($marker));
            $pieces = explode(",",ltrim($rest));
            $ltemodem->ip_address = preg_split('/\\r\\n|\\r|\\n/ ',ltrim($pieces[1]))[0];



            if(filter_var($ltemodem->ip_address) && ( $ltemodem->ip_address !== $savedconfig["IP"] ) )
            {
                $savedconfig["IP"] = $ltemodem->ip_address;
                $changed=1;
            }

        }

        /*
        at+csq
        +csq: 19,99
        this will dispay the signal strength
        */
        $serial->sendMessage("at+csq\r");
        $read = $serial->readPort();
        $marker = '+csq:';
        $pos = strpos($read, $marker);
        $signal_ok = 0;
        if ($pos != false) {
            $rest = substr($read, $pos+strlen($marker));
            $pieces = preg_split('/\\r\\n|\\r|\\n/ ',ltrim($rest));
            $ltemodem->signal_strength = $pieces[0];

            # <10 - marginal , 10 - <15 - ok , 15 - <19 - good, >= 20 excellent

            if(!empty($ltemodem->signal_strength) && ( $ltemodem->signal_strength !== $savedconfig["signal_strength"] ) )
            {
                $savedconfig["signal_strength"] = $ltemodem->signal_strength;
                $changed=1;
            }
        }


        $serial->sendMessage("at!gpsloc?\r");
        $read = $serial->readPort();


        $marker = 'OK';
        $pos = strpos($read, $marker);
        if ($pos != false) {

            $pieces1 = explode("OK", $read);
            $pieces =  explode("at!gpsloc?",$pieces1[0]);
            //$pieces = preg_split('/\\r\\n|\\r|\\n/ ',ltrim($rest));
            if(!empty($pieces[1]))
            {
                $ltemodem->gps = $pieces[1];
            }
            else
            {
                $ltemodem->gps = "Not Available" ;
            }

        }
        if(!empty($ltemodem->gps) && ( $ltemodem->gps !== $savedconfig["GPS"] ) )
        {
            $savedconfig["GPS"] = $ltemodem->gps;
            $changed=1;
        }
        if($changed){ file_put_contents("panel.conf", serialize($savedconfig)); }
    }
    else if($GLOBALS['modem_type'] == 'ec25ql')
    {
        //NEEDTOIMPLENT
    }

}

function loadLTEModem() {
	global $dev;
	global $ltemodem;
	global $serial;
	global $savedconfig;

	// First we must specify the device. This works on both linux and windows (if
	// your linux serial device is /dev/ttyS0 for COM1, etc)
	if ($serial->deviceSet($dev) === true) {

		// Then we need to open it
		$serial->deviceOpen();

		get_modem_type();
		get_modem_status();

		// Update the rest information
		if ($ltemodem->status === 'READY') {
			LTE_MODEM_INFORMATION();
		} else {
			$savedconfig['IP'] = '';
			$savedconfig['signal_strength'] = $savedconfig['APN'] = $savedconfig['Network'] = $savedconfig['BAND'] = $savedconfig['BANDWIDTH'] = '';
		}
	}
	else {
		$ltemodem->status = 'MODEM DRIVERS NOT PRESENT';
		$savedconfig=array();
	}

	$serial->deviceClose();
}

function saveLTEModem() {
	/* TODO: */

	$savedconfig=unserialize(file_get_contents('panel.conf'));

	if($savedconfig["APN"] !== $_POST["APN"])
	{
		$newapn=$_POST["APN"];
		cmd("at+cgdcont=1,\"IP\",\"$newapn\"");
		$savedconfig["APN"] = $_POST["APN"];
		print "SAVEPOST: ";
		var_dump($savedconfig);
		file_put_contents("panel.conf", serialize($savedconfig));
    }

}

function reboot() {
	clear_modem_status();
	$lineline = system('reboot', $retval);
}

function updateSW() {

}

function todo() {
	echo 'TODO ... ';
}


class config {
    //this function parses a config file
    public function parse_config($file){
        return parse_ini_file($file);
    }
    //this function writes or edits a config file
    //defining a var quickly
    public function edit_config($property,$equals,$file){
        //fetch content of the file
        $content = file_get_contents($file);
        //splits the content into an array by line break
        $lines = explode("\n",$content);
        //defines a array for new lines, throwing edited one in the mix
        $newLines = array();
        //counter
        $count = 0;
        //loops through the array/lines
        foreach($lines as $value) {
            //length of specified property name
            $propLength = strlen($property);
            //check if the beginning of line is equal to the property name
            if(substr($value,0,$propLength) == $property){
                //if it is add the new value to the new line array
                $newLines[$count] = "$property=$equals";
            }else {
                //if not just add the original line
                $newLines[$count] = $value;
            }
            //increment the count
            $count= $count+1;
        }
        $final;
        //loop through the newLines array, and append it to the final string with a line break
        foreach($newLines as $i){
        //is so extra lines arent added at top of file
            if(!isset($final)){
                $final = $i;
            }else {
                $final .= "\n".$i;
            }
        }
        //write the new file
        $write = fopen($file,"w");
        fwrite($write,$final);
        fclose($write);
    }
}

$host_conf = '/etc/hostapd.conf';
$config = new config();
$wifi = '';
function loadWifi() {
	/* TODO: */
	global $wifi;
	global $host_conf;
	global $config;
	$wifi = $config->parse_config($host_conf);
}

function saveWifi($ssdi, $psk) {
	global $config;
	global $host_conf;
	$config->edit_config('ssid', $ssdi, $host_conf);
	$config->edit_config('wpa_passphrase', $psk, $host_conf);
}

?>

<html>
<head>
<meta http-equiv="refresh" content="15">
<title>SCADA WEB PANEL</title>
<style>

input[type=text], input[type=password], select, textarea {
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    resize: vertical;
    font-size: 14px;
}

button, input[type=submit] {
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    resize: vertical;
    font-size: 16px;
}

label {
    padding: 12px 12px 12px 0;
    display: inline-block;
}

.container-full {
    background-color: #f2f2f2;
	float: left;
	width: 100%;

}

.container {
    background-color: #f2f2f2;
	width: 50%;
	float: left;
}

.input-right {
    background-color: #E3AD1E;
    color: black;
    padding: 12px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
	float: right;
	margin-top: 6px;
	margin-right: 6px;
}

.input-left {
    background-color: #E3AD1E;
    color: black;
    padding: 12px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
	float: left;
	margin-top: 6px;
	margin-left: 6px;
}


.error { border: 1px solid #f00; }

.input-edit {
    background-color: #42a7f4;
    color: black;
    padding: 12px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
	width: 30%;
}

.input-send {
    background-color: #42a7f4;
    color: black;
    border: none;
    border-radius: 4px;
    cursor: pointer;
	width: 20%;
	margin-top: 9px;
	padding: 7 0 7 0;
}

.input-pass {
    background-color: #42a7f4;
    color: black;
    border: none;
    border-radius: 4px;
    cursor: pointer;
	width: 7%;
	margin-top: 9px;
	padding: 10 0 10 0;
}


.input-save {
    background-color: #d8ecff;
    color: black;
    padding: 12px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
	width: 30%;
}

.input-right:hover {
    background-color: #45a049;
}

.input-left:hover {
    background-color: #45a049;
}

.input-edit:hover {
    background-color: #45a049;
}

.input-save:hover {
    background-color: #45a049;
}
.col-50-left {
	color: blue;
	text-align: center;
    float: left;
    width: 40%;
    margin-top: 6px;
    font-size: 18px;
}
.col-50-right {
	color: blue;
	text-align: center;
    float: right;
    width: 40%;
    margin-top: 6px;
    font-size: 18px;
}

.eth {
	text-align: center;
	color: blue;
	font-size: 18px;
}

.col-20 {
    float: left;
    width: 20%;
    margin-top: 6px;
}


.col-60 {
    float: left;
    width: 60%;
    margin-top: 6px;
}

.col-30 {
    float: left;
    width: 30%;
    margin-top: 6px;
}

.col-98-green {
	background-color: green;
    width: 98%;
    margin-top: 6px;
}
.col-98-red {
	background-color: red;
    width: 98%;
    margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
    .col-25, .col-75, input[type=submit] {
        width: 100%;
        margin-top: 0;
    }
}
</style>
<script src="jquery.js">
</script>
<script>

function updateSW() {
	window.stop();
	var ver = $("#vers").text().split(" ");
//	alert(ver[1]);
	$('#vers').html('<img src="wait.gif" width="16px">');
	$('#vers').load('update.php');
	window.location.reload();
}

function validateEditBox(element, name, errname)
{
	if( /[^a-zA-Z0-9]/.test( element) ) {
      	$("#"+errname).html('Please enter only alphabet and numbers to ' + name + ' !');
      	$("#"+name).css("border", "1px solid red");
     	//  $("#"+name).addClass("error");
       	return false;
    }

    if(element.length < 8 || element.length > 12 )
    {
		$("#"+errname).html('Please enter ' + name + ' name between 8 and 12 symbols !');
      	$("#"+name).css("border", "1px solid red");
     	//  $("#"+name).addClass("error");
       	return false;
    }

	$("#"+name).css("border", "0px");
    $("#errdesc").html('');

	return true;
}


function validateCode(){
    var ssid = document.getElementById('ssid').value;

    if (validateEditBox(ssid, 'ssid', 'errdesc') === false)
		return false;

    var psk = document.getElementById('psk').value;

    if (validateEditBox(psk, 'psk', 'errdesc') === false)
		return false;

    return true;
 }

 function at()
 {
 // alert('at');
 window.stop();
  var cmdr = document.getElementById('atcmd').value;
  cmdr = cmdr.replace('+', 'plus');
 // alert(cmdr);
  $('#atcmdrez').html('<img src="wait.gif" width="16px">');
 $('#atcmdrez').load('at.php?c='+cmdr);
 }

function chpass()
{

	var newpass= document.getElementById('newpass').value;

	    if( /[^a-zA-Z0-9]/.test(newpass) ) {
      $("#passrez").html('Please enter only alphabet and numbers to new password !');
      $("#newpass").css("border", "1px solid red");
       return false;
    }

        if(newpass.length < 8 || newpass.length > 12 )
    {
    	      $("#passrez").html('Please enter new password between 8 and 12 symbols !');
      $("#newpass").css("border", "1px solid red");

       return false;
    }

   $('#passbox').hide();
   $('#passrez').html('<img src="wait.gif" width="16px">');
   $('#passrez').load("updpass.php?p="+newpass);

  return false;
}

 </script>

</head>

<body>
<?php  if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	?>
 <?php
 loadWifi();
 loadLTEModem();
 ?>

 <div class="container-full" >
 <div class="row">
 <label id="vers" style="float: right ; top: 6; right: 250; position: absolute;">
 <a href="" onclick="window.stop(); $('#passbox').toggle(); return false; " style="border: 1px dotted #e5e5e5;">Change Password</a>
 &nbsp;&nbsp;&nbsp;&nbsp;<a href="logout.php" >Logout</a></label>
	<form action="<?php echo $ltemodem->_SERVER['PHP_SELF'] ?>" method="POST">
		<input class="input-right" type="submit" name="update" value="UPDATE SOFTWARE" onClick="updateSW()">
		<center><label><font color="orange" style="font-size: 40px;">&nbsp;&nbsp;&nbsp;&nbsp;<b>webPanel</b></font>	</label>
		<input class="input-left" type="submit" name="reboot" value="REBOOT" onClick="reboot()">
	</form>
 </div>
 		<div width="600" align="center" id="passbox" style="display: none;">
<label for="newpass">NEW PASSWORD : </label>
<input align="center" style="text-align: center" type="password" id="newpass" name="newpass" value="" > &nbsp;&nbsp; &nbsp; <button class="input-pass"  onclick="chpass(); return false;">Change</button>
    </div>
    <div width="600" align="center" id="passrez"></div>
 <div class="row">
	<div class="col-50-left">
        <label>WIFI</label>
    </div>
	<div class="col-50-right">
        <label>LTE MODEM</label>
    </div>
 </div>
 <div class="row">
	 <div class="container"><fieldset style="border: 1px dotted #bbb; padding: 30px;">
	 <div align="center" style="font-color: red">
	 <label id="errdesc" style="color: red; font-color: red;"></label></div>
	 <form action="<?php echo $ltemodem->_SERVER['PHP_SELF'] ?>" method="POST"  onsubmit="return validateCode();" >
	 <div class="row">
	 <label class="col-20" for="ssid">SSID</label>
	 <input style="text-align: center;" class="col-60" type="text" id="ssid" name="ssid" value="<?php echo $wifi['ssid']; ?>" readonly="readonly" /><br />
	 </div>
	 <div class="row">
	 <label class="col-20" for="psk">PSK</label>
	 <input style="text-align: center;" class="col-60" type="password" id="psk" name="psk" value="<?php echo $wifi['wpa_passphrase']; ?>" readonly="readonly" /><br />
	 </div>
	 </br>
	 <div align="center">
	 <button class="input-edit" type="button" onclick="window.stop();  document.getElementById('ssid').focus(); editWifi()">Edit</button>
	 <input class="input-save" type="submit" value="Save" />
	 </div>
	 </form></fieldset>
	 <br><br><fieldset style="border: 1px dotted #bbb; padding: 30px;">
	 <div class="row">
	 	<div class="eth">
        <label>ETHERNET</label>
    </div>
    </div>
	 	 <div class="row">
	 <label class="col-20" for="psk">IP ADDRESS</label>
	 <input style="text-align: center;" class="col-60" type="text" id="eth_ip" name="ETH_IP" value="<?php  echo $ethIP ?>" readonly="readonly"><br />
	 </div></fieldset>
	 </div>

	 <div class="container"><fieldset style="border: 1px dotted #bbb; padding: 30px;">
	 <form action="<?php echo $ltemodem->_SERVER['PHP_SELF'] ?>" method="POST">
	 <div class="row">
	 <label class="col-30" for="status">STATUS</label>
     <input class="col-60" type="text" id="status" name="STATUS" <?php if($ltemodem->status == 'MODEM DRIVERS NOT PRESENT'){ ?> style="color: #f00;" <?php } ?> value="<?php echo $ltemodem->status ?>" readonly="readonly">
     </div>
	 <div class="row">
	 <label class="col-30" for="status">IP ADDRESS</label>
     <input class="col-60" type="text" id="ip_address" name="IP" value="<?php echo $savedconfig["IP"]; ?>" readonly="readonly">
     </div>
	 <div class="row">
	 <label class="col-30" for="signal_strength">SIGNAL STRENGTH</label>
     <input class="col-60" type="text" id="signal_strength" name="signal_strength" value="<?php echo csq_human_readible( $savedconfig["signal_strength"] ); ?>" readonly="readonly">
     </div>
	 <div class="row">
	 <label class="col-30" for="apn">APN</label>
     <input class="col-60" type="text" id="apn" name="APN" value="<?php  echo $savedconfig["APN"]; ?>" readonly="readonly">
     </div>
     <div class="row">
	 <label class="col-30" for="network">NETWORK</label>
     <input class="col-60" type="text" id="network" name="Network" value="<?php  echo $savedconfig["Network"]; ?>" readonly="readonly">
     </div>
	 <div class="row">
	 <label class="col-30" for="band">BAND</label>
     <input class="col-60" type="text" id="band" name="BAND" value="<?php echo $savedconfig["BAND"] ?>" readonly="readonly">
     </div>
	 <div class="row">
	 <label class="col-30" for="bw">BANDWIDTH</label>
     <input class="col-60" type="text" id="bw" name="BANDWIDTH" value="<?php echo $savedconfig["BANDWIDTH"] ?>" readonly="readonly">
     </div>
	 <div class="row">
	 <label class="col-30" for="gps">GPS</label>
     <input class="col-60" type="text" id="band" name="GPS" value="<?php  echo $savedconfig["GPS"] ?>" readonly="readonly">
     </div>
	 <?php
	 	$marker = 'LTE';
	    $pos = strpos($savedconfig["Network"] , $marker);
	    if ($pos = false) { ?>
	 <div align="center">
	 <button class="input-edit" type="button" onclick="window.stop(); document.getElementById('apn').focus(); editAPN()">EditAPN</button>
	 <input class="input-save" type="submit" value="SaveAPN" />
	 </div>
	 <?php } ?>
	 </form>
	      <hr>
     	 <div class="row">
	 <label class="col-30" for="atcmd">AT COMMAND</label>
     <input class="col-30" align="left" type="text" id="atcmd" name="atcmd" value="" > &nbsp;&nbsp; &nbsp; <button class="input-send" onclick="at(); return false;">SEND</button>
     <div id="atcmdrez" style="background-color: #f9f9f9; margin-top: 15px;"></div>
     </div>
	 </fieldset>
	 </div>
 </div>
 </div>


<?php }
else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if (isset($_POST['ssid']) && isset($_POST['psk'])) {
		saveWifi($_POST['ssid'], $_POST['psk']);
	}

	else if (    isset($_POST['STATUS'])) {

		saveLTEModem();

	}
	else if ($_POST['update']){
		 updateSW();
	}
	else if ($_POST['reboot']){
		 reboot();
	}

	loadWifi();
	loadLTEModem();
    ?>

  <div class="container-full">
  <div class="row">
   <label id="vers" style="float: right ; top: 6; right: 250; position: absolute;">
 <a href="" onclick="window.stop(); $('#passbox').toggle(); return false; " style="border: 1px dotted #e5e5e5;">Change Password</a>
 &nbsp;&nbsp;&nbsp;&nbsp;<a href="logout.php" >Logout</a></label>

	<form action="<?php echo $ltemodem->_SERVER['PHP_SELF'] ?>" method="POST">
		<input class="input-right" type="button" name="update" value="UPDATE SOFTWARE" onClick="updateSW()">
		<center><label><font color="orange" style="font-size: 40px;">&nbsp;&nbsp;&nbsp;&nbsp;<b>webPanel</b></font>	</label>
		<input class="input-left" type="button" name="reboot" value="REBOOT" onClick="reboot()">
	</form>
  </div>
   		<div width="600" align="center" id="passbox" style="display: none;">
<label for="newpass">NEW PASSWORD : </label>
<input align="center" style="text-align: center" type="password" id="newpass" name="newpass" value="" > &nbsp;&nbsp; &nbsp; <button class="input-pass"  onclick="chpass(); return false;">Change</button>
    </div>
    <div width="600" align="center" id="passrez"></div>
 <div class="row">
	<div class="col-50-left">
        <label>WIFI</label>
    </div>
	<div class="col-50-right">
        <label>LTE MODEM</label>
    </div>
 </div>
 <div class="row">
	 <div class="container"><fieldset style="border: 1px dotted #bbb; padding: 30px;">
	 <form action="<?php echo $ltemodem->_SERVER['PHP_SELF'] ?>" method="POST">
	 <div class="row">
	 <label class="col-20" for="ssid">SSID</label>
	 <input style="text-align: center;" class="col-60" type="text" id="ssid" name="ssid" value="<?php echo $wifi['ssid']; ?>" readonly="readonly" /><br />
	 </div>
	 <div class="row">
	 <label class="col-20" for="psk">PSK</label>
	 <input style="text-align: center;" class="col-60" type="password" id="psk" name="psk" value="<?php echo $wifi['wpa_passphrase']; ?>" readonly="readonly" /><br />
	 </div>
	 </br>
	 <div align="center">
	 <button class="input-edit" type="button" onclick="window.stop();  document.getElementById('ssid').focus(); editWifi()">Edit</button>
	 <input class="input-save" type="submit" value="Save" />
	 </div>
	 </form>
</fieldset>
	 <br><br><fieldset style="border: 1px dotted #bbb; padding: 30px;">
	 <div class="row">
	 	<div class="eth">
        <label>ETHERNET</label>
    </div>
    </div>
	 	 <div class="row">
	 <label class="col-20" for="psk">IP ADDRESS</label>
	 <input style="text-align: center;" class="col-60" type="text" id="eth_ip" name="ETH_IP" value="<?php  echo $ethIP ?>" readonly="readonly"><br />
	 </div></fieldset>
	 </div>

	 <div class="container"><fieldset style="border: 1px dotted #bbb; padding: 30px;">
	 <form action="<?php echo $ltemodem->_SERVER['PHP_SELF'] ?>" method="POST">
	 <div class="row">
	 <label class="col-30" for="status">STATUS</label>
     <input class="col-60" type="text" id="status" name="STATUS" value="<?php  echo $ltemodem->status ?>" readonly="readonly">
     </div>
	 <div class="row">
	 	 <label class="col-30" for="status">IP ADDRESS</label>
     <input class="col-60" type="text" id="ip_address" name="IP" value="<?php echo $savedconfig["IP"]; ?>" readonly="readonly">
     </div>
	 <div class="row">
	 <label class="col-30" for="signal_strength">SIGNAL STRENGTH</label>
     <input class="col-60" type="text" id="signal_strength" name="signal_strength" value="<?php echo $savedconfig["signal_strength"]; ?>" readonly="readonly">
     </div>
	 <div class="row">
	 <label class="col-30" for="apn">APN</label>
     <input class="col-60" type="text" id="apn" name="APN" value="<?php  echo $savedconfig["APN"]; ?>" readonly="readonly">
     </div>
     <div class="row">
	 <label class="col-30" for="network">NETWORK</label>
     <input class="col-60" type="text" id="network" name="Network" value="<?php  echo $savedconfig["Network"] ; ?>" readonly="readonly">
     </div>
	 <div class="row">
	 <label class="col-30" for="band">BAND</label>
     <input class="col-60" type="text" id="band" name="BAND" value="<?php echo $savedconfig["BAND"] ?>" readonly="readonly">
     </div>
	 <div class="row">
	 <label class="col-30" for="bw">BANDWIDTH</label>
     <input class="col-60" type="text" id="bw" name="BANDWIDTH" value="<?php echo $savedconfig["BANDWIDTH"] ?>" readonly="readonly">
     </div>
	 	 <div class="row">
	 <label class="col-30" for="gps">GPS</label>
     <input class="col-60" type="text" id="band" name="GPS" value="<?php  echo $savedconfig["GPS"] ?>" readonly="readonly">
     </div>
	 <?php
	 	$marker = 'LTE';
	    $pos = strpos($savedconfig["Network"] , $marker);
	    if ($pos = false) { ?>
	 <div align="center">
	 <button class="input-edit" type="button" onclick="window.stop(); document.getElementById('apn').focus(); editAPN()">EditAPN</button>
	 <input class="input-save" type="submit" value="SaveAPN" />
	 </div>
	 <?php } ?>
	 </form>
	      <hr>
     	 <div class="row">
	 <label class="col-30" for="atcmd">AT COMMAND</label>
     <input class="col-30" align="left" type="text" id="atcmd" name="atcmd" value="" > &nbsp;&nbsp; &nbsp; <button class="input-send" onclick="at(); return false;">SEND</button>
     <div id="atcmdrez" style="background-color: #f9f9f9; margin-top: 15px;"></div>
     </div>
	 </fieldset>
	 </div>
 </div>
 </div>

<?php }
else {
 die("This script only works with GET and POST requests.");
} ?>
<table width="100%" style="padding:0;" >
<tr style="padding:0;"><td style="padding:0;">

<label id="vers" style="padding: 0 0 0 180px;">scada update version: <?php if(file_exists("/etc/update_pack_version")){ echo file_get_contents("/etc/update_pack_version"); } else { echo "UNKNOWN"; } ?> &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
<br>
<label id="build" style="padding: 0 0 0 180px;">scada builds version: <?php if(strpos(php_uname(), "SCADA") !== false){$uname_words = explode(" ", php_uname()); echo $uname_words[0]; } else { echo "UNKNOWN"; } ?> &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
</td><td> </td></tr>
<tr style="padding:0;"><td style="padding:0;"><label style="padding: 0 0 0 180px;">&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.ssla.co.uk" target="_blank">www.ssla.co.uk</a></label></td><td></td></tr>
</table>
</body>

<script>
function editWifi() {
	window.stop();
	document.getElementById('ssid').removeAttribute('readonly');
	document.getElementById('psk').removeAttribute('readonly');
}
function editAPN() {
	window.stop();
	document.getElementById('apn').focus();
	document.getElementById('apn').removeAttribute('readonly');
}

</script>
</html>
